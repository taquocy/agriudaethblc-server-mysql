var db = require('../../config/database');
var dbFunc = require('../../config/db-function');

var ProductModel = {
   getAllProduct:getAllProduct,
   addProduct:addProduct,
   updateProduct:updateProduct,
   deleteProduct:deleteProduct,
   getProductById:getProductById
}

function getAllProduct() {
    return new Promise((resolve,reject) => {
        db.query(`SELECT * FROM agriudaethblc.product`,(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
       });
    });
}

function getProductById(id) {
    return new Promise((resolve,reject) => {
        db.query("SELECT * FROM test WHERE id ="+id.id,(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
       });
    });  
}

function addProduct(Product) {
     return new Promise((resolve,reject) => {
         db.query("INSERT INTO test(name,age,state,country)VALUES('"+Product.name+"','"+Product.age+"','"+Product.state+"','"+Product.country+"')",(error,rows,fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
          });
        });
}


function updateProduct(id,Product) {
    return new Promise((resolve,reject) => {
        db.query("UPDATE test set name='"+Product.name+"',age='"+Product.age+"',state='"+Product.state+"',country='"+Product.country+"' WHERE id='"+id+"'",(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
       });    
    })
}

function deleteProduct(id) {
   return new Promise((resolve,reject) => {
        db.query("DELETE FROM test WHERE id='"+id+"'",(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
       });    
    });
}


module.exports = ProductModel;

